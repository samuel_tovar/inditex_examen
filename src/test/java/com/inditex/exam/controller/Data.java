package com.inditex.exam.controller;

import com.inditex.exam.model.entity.Price;
import com.inditex.exam.model.enums.MoneyType;
import com.inditex.exam.model.request.PriceRequest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

public class Data {
    static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static PriceRequest getRequestNotFound(){
        return PriceRequest.builder()
                .brandId(1L)
                .productId(35455L)
                .startDate(LocalDateTime.parse("2021-06-14 20:00:00",formatter))
                .build();
    }

    public static PriceRequest getRequest001(){
        return PriceRequest.builder()
                .brandId(1L)
                .productId(35455L)
                .startDate(LocalDateTime.parse("2020-06-14 10:00:00",formatter))
                .build();
    }

    public static PriceRequest getRequest002(){
        return PriceRequest.builder()
                .brandId(1L)
                .productId(35455L)
                .startDate(LocalDateTime.parse("2020-06-14 16:00:00",formatter))
                .build();
    }

    public static PriceRequest getRequest003(){
        return PriceRequest.builder()
                .brandId(1L)
                .productId(35455L)
                .startDate(LocalDateTime.parse("2020-06-14 21:00:00",formatter))
                .build();
    }

    public static PriceRequest getRequest004(){
        return PriceRequest.builder()
                .brandId(1L)
                .productId(35455L)
                .startDate(LocalDateTime.parse("2020-06-15 10:00:00",formatter))
                .build();
    }

    public static PriceRequest getRequest005(){
        return PriceRequest.builder()
                .brandId(1L)
                .productId(35455L)
                .startDate(LocalDateTime.parse("2020-06-16 21:00:00",formatter))
                .build();
    }

    public static List<Price > getAllPrice() {
        return Arrays.asList(
                Price.builder()
                        .id(1L)
                        .brandId(1L)
                        .startDate(LocalDateTime.parse("2020-06-14 00:00:00",formatter))
                        .endDate(LocalDateTime.parse("2020-12-31 23:59:59",formatter))
                        .priceList(1L)
                        .productId(35455L)
                        .priority(0L)
                        .price(35.50)
                        .curr(MoneyType.EUR)
                        .build(),
                Price.builder()
                        .id(2L)
                        .brandId(1L)
                        .startDate(LocalDateTime.parse("2020-06-14 15:00:00",formatter))
                        .endDate(LocalDateTime.parse("2020-06-14 18:30:00",formatter))
                        .priceList(2L)
                        .productId(35455L)
                        .priority(1L)
                        .price(25.45)
                        .curr(MoneyType.EUR)
                        .build(),
                Price.builder()
                        .id(3L)
                        .brandId(1L)
                        .startDate(LocalDateTime.parse("2020-06-15 00:00:00",formatter))
                        .endDate(LocalDateTime.parse("2020-06-15 11:00:00",formatter))
                        .priceList(3L)
                        .productId(35455L)
                        .priority(1L)
                        .price(30.50)
                        .curr(MoneyType.EUR)
                        .build(),
                Price.builder()
                        .id(4L)
                        .brandId(1L)
                        .startDate(LocalDateTime.parse("2020-06-15 16:00:00",formatter))
                        .endDate(LocalDateTime.parse("2020-12-31 23:59:59",formatter))
                        .priceList(4L)
                        .productId(35455L)
                        .priority(1L)
                        .price(38.95)
                        .curr(MoneyType.EUR)
                        .build()
        );
    }
}
