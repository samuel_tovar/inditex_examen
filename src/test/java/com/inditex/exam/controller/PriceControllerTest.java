package com.inditex.exam.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.inditex.exam.model.dto.PriceDto;
import com.inditex.exam.repository.PriceRepository;
import com.inditex.exam.service.PriceService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static com.inditex.exam.controller.Data.getAllPrice;
import static com.inditex.exam.controller.Data.getRequest001;
import static com.inditex.exam.controller.Data.getRequest002;
import static com.inditex.exam.controller.Data.getRequest003;
import static com.inditex.exam.controller.Data.getRequest004;
import static com.inditex.exam.controller.Data.getRequest005;
import static com.inditex.exam.controller.Data.getRequestNotFound;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
public class PriceControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PriceService priceService;

    @MockBean
    private PriceRepository priceRepository;

    ObjectMapper objectMapper;

    @BeforeEach
    public void before(){
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
    }

    @Test
    public void testGetPriceNotFound() throws Exception {
        when(priceRepository.findAll()).thenReturn(getAllPrice());

        String json = objectMapper.writeValueAsString(getRequestNotFound());
        mockMvc.perform(get("/api/v1/final_price").contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetPriceExceptionGlobal() throws Exception {

        mockMvc.perform(get("/api/v1/final_price").contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void testGetPrice001() throws Exception {
        when(priceRepository.findAll()).thenReturn(getAllPrice());

        String json = objectMapper.writeValueAsString(getRequest001());
        String result = mockMvc.perform(get("/api/v1/final_price").contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpectAll(
                        status().isOk(),
                        jsonPath("$.brand_id").value(1),
                        jsonPath("$.price").value(35.50),
                        jsonPath("$.product_id").value(35455L)
                ).andReturn().getResponse().getContentAsString();

        PriceDto priceDto = objectMapper.readValue(result,PriceDto.class);

        verify(priceRepository).findAll();
        Assertions.assertEquals(getAllPrice().get(0).getPrice(),priceDto.getPrice());
    }

    @Test
    public void testGetPrice002() throws Exception {
        when(priceRepository.findAll()).thenReturn(getAllPrice());

        String json = objectMapper.writeValueAsString(getRequest002());

        String result = mockMvc.perform(get("/api/v1/final_price").contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpectAll(
                        status().isOk(),
                        jsonPath("$.brand_id").value(1),
                        jsonPath("$.price").value(25.45),
                        jsonPath("$.product_id").value(35455L)
                ).andReturn().getResponse().getContentAsString();

        PriceDto priceDto = objectMapper.readValue(result,PriceDto.class);

        verify(priceRepository).findAll();
        Assertions.assertEquals(getAllPrice().get(1).getPrice(),priceDto.getPrice());
    }

    @Test
    public void testGetPrice003() throws Exception {
        when(priceRepository.findAll()).thenReturn(getAllPrice());

        String json = objectMapper.writeValueAsString(getRequest003());
        String result = mockMvc.perform(get("/api/v1/final_price").contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpectAll(
                        status().isOk(),
                        jsonPath("$.brand_id").value(1),
                        jsonPath("$.price").value(35.50),
                        jsonPath("$.product_id").value(35455L)
                ).andReturn().getResponse().getContentAsString();

        PriceDto priceDto = objectMapper.readValue(result,PriceDto.class);

        verify(priceRepository).findAll();
        Assertions.assertEquals(getAllPrice().get(0).getPrice(),priceDto.getPrice());
    }

    @Test
    public void testGetPrice004() throws Exception {
        when(priceRepository.findAll()).thenReturn(getAllPrice());

        String json = objectMapper.writeValueAsString(getRequest004());
        String result = mockMvc.perform(get("/api/v1/final_price").contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpectAll(
                        status().isOk(),
                        jsonPath("$.brand_id").value(1),
                        jsonPath("$.price").value(30.50),
                        jsonPath("$.product_id").value(35455L)
                ).andReturn().getResponse().getContentAsString();

        PriceDto priceDto = objectMapper.readValue(result,PriceDto.class);

        verify(priceRepository).findAll();
        Assertions.assertEquals(getAllPrice().get(2).getPrice(),priceDto.getPrice());
    }

    @Test
    public void testGetPrice005() throws Exception {
        when(priceRepository.findAll()).thenReturn(getAllPrice());

        String json = objectMapper.writeValueAsString(getRequest005());
        String result = mockMvc.perform(get("/api/v1/final_price").contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpectAll(
                        status().isOk(),
                        jsonPath("$.brand_id").value(1),
                        jsonPath("$.price").value(38.95),
                        jsonPath("$.product_id").value(35455L)
                ).andReturn().getResponse().getContentAsString();

        PriceDto priceDto = objectMapper.readValue(result,PriceDto.class);

        verify(priceRepository).findAll();
        Assertions.assertEquals(getAllPrice().get(3).getPrice(),priceDto.getPrice());
    }
}
