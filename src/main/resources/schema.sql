create table if not exists PRICES (
        ID bigint not null,
        BRAND_ID bigint,
        CURR VARCHAR(10),
        END_DATE timestamp,
        PRICE double,
        PRICE_LIST bigint,
        PRIORITY bigint,
        PRODUCT_ID bigint,
        START_DATE timestamp,
        primary key (ID)
    );
