package com.inditex.exam.service;

import com.inditex.exam.model.dto.PriceDto;
import com.inditex.exam.model.request.PriceRequest;

public interface PriceService {
	PriceDto getPrice(PriceRequest priceRequest);
}
