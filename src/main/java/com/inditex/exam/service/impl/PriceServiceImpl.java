package com.inditex.exam.service.impl;

import com.inditex.exam.exception.PriceNotFoundException;
import com.inditex.exam.model.dto.PriceDto;
import com.inditex.exam.model.entity.Price;
import com.inditex.exam.model.mapper.PriceMapper;
import com.inditex.exam.model.request.PriceRequest;
import com.inditex.exam.repository.PriceRepository;
import com.inditex.exam.service.PriceService;

import java.util.Comparator;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PriceServiceImpl implements PriceService {

	private final PriceRepository priceRepository;
	private final PriceMapper mapper;

	@Autowired
	public PriceServiceImpl(final PriceRepository priceRepository,
		final PriceMapper mapper) {
		this.priceRepository = priceRepository;
		this.mapper = mapper;
	}

	@Override
	public PriceDto getPrice(PriceRequest priceRequest){
		return getPriceFirst(priceRequest)
				.orElseThrow(() -> new PriceNotFoundException("Price not Found"));
	}

	private Optional<PriceDto> getPriceFirst(PriceRequest priceRequest){
		return  priceRepository.findAll()
			.stream()
			.filter(p-> p.getStartDate().isBefore(priceRequest.getStartDate())
					&& p.getEndDate().isAfter(priceRequest.getStartDate())
			)
			.max(Comparator.comparing(Price::getPriority))
			.map(mapper::toDto);
	}
}
