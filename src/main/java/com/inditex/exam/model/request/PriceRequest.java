package com.inditex.exam.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PriceRequest {
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonProperty("start_date")
	private LocalDateTime startDate;

	@JsonProperty("brand_id")
	private Long brandId;

	@JsonProperty("product_id")
	private Long productId;
}
