package com.inditex.exam.model.mapper;

import com.inditex.exam.model.dto.PriceDto;
import com.inditex.exam.model.entity.Price;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PriceMapper {

    PriceDto toDto(Price target);

    @Mappings({
        @Mapping(ignore = true, target = "priceList"),
        @Mapping(ignore = true, target = "priority")
    })
    Price toModel(PriceDto source);
}
