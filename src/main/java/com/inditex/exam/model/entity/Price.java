package com.inditex.exam.model.entity;

import com.inditex.exam.model.enums.MoneyType;
import java.time.LocalDateTime;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="PRICES")
public class Price {
    @Id @GeneratedValue
    @Column(name ="ID")
    private Long id;

    @Column(name = "BRAND_ID")
    private Long brandId;
    @Column(name = "START_DATE")
    private LocalDateTime startDate;
    @Column(name = "END_DATE")
    private LocalDateTime endDate;
    @Column(name = "PRICE_LIST")
    private Long priceList;
    @Column(name = "PRODUCT_ID")
    private Long productId;
    @Column(name = "PRIORITY")
    private Long priority;
    @Column(name = "PRICE")
    private Double price;
    @Column(name = "CURR")
    @Enumerated(value = EnumType.STRING)
    private MoneyType curr;
}
