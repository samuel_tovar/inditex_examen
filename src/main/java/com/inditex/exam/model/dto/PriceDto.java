package com.inditex.exam.model.dto;

import com.inditex.exam.model.enums.MoneyType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PriceDto {
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonProperty("start_date")
	private LocalDateTime startDate;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonProperty("end_date")
	private LocalDateTime endDate;
	@JsonProperty("brand_id")
	private Long brandId;
	@JsonProperty("product_id")
	private Long productId;
	private Long priceList;
	private Long priority;
	private Double price;
	private MoneyType curr;
}
