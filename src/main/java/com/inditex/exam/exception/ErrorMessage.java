package com.inditex.exam.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ErrorMessage {
        private int statusCode;
        private Date timestamp;
        private String message;
        private String description;
}
