package com.inditex.exam.controller;

import com.inditex.exam.model.dto.PriceDto;
import com.inditex.exam.model.request.PriceRequest;
import com.inditex.exam.service.PriceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/v1/")
public class PriceController {

	private final PriceService priceService;

	@Autowired
	public PriceController(final PriceService priceService) {
		this.priceService = priceService;
	}

	@GetMapping("final_price")
	public ResponseEntity<PriceDto> converter(@Validated @RequestBody PriceRequest inputData) {
		log.info("Request {}",inputData);
		return ResponseEntity.ok().body(priceService.getPrice(inputData));
	}

}
