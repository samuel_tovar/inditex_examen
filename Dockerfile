FROM gradle:latest as build
WORKDIR /app
COPY --chown=gradle:gradle src src
COPY --chown=gradle:gradle build.gradle .
COPY --chown=gradle:gradle settings.gradle .
RUN gradle assemble --no-daemon
#COPY gradle $APP_HOME/gradle
#RUN ./gradlew build || return 0
#COPY . .
#RUN ./gradlew build


FROM openjdk:11-slim-buster
RUN mkdir /app
WORKDIR /app
COPY --from=build /app/build/libs/exam-skeleton-java-0.0.1.jar ./
ENTRYPOINT [ "java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "exam-skeleton-java-0.0.1.jar"]
